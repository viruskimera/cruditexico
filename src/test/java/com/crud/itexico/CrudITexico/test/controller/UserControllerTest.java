/**
 * 
 */
package com.crud.itexico.CrudITexico.test.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.dao.DataIntegrityViolationException;

import com.crud.itexico.CrudITexico.Exception.UserNotFoundException;
import com.crud.itexico.CrudITexico.controller.UserController;
import com.crud.itexico.CrudITexico.model.User;
import com.crud.itexico.CrudITexico.service.UserService;

/**
 * Unit testing class for the UserController
 * @author viruskimera
 *
 */
public class UserControllerTest {
	
	private UserController controller;
	
	private UserService service;
	
	private User validUser;
	/**
	 * The setup method
	 */
	@Before
	public void setup(){
		validUser = new User();
		setValidUser(validUser);
		controller =  new UserController();
		service =  Mockito.mock(UserService.class);
		controller.setService(service);
	}
	
	@Test
	public void successCase(){
		
		Mockito.
			when(service.createUser(ArgumentMatchers.any(User.class))).
				thenReturn(validUser);

		assertTrue(controller.createUser(validUser).toString().contains("200"));
		assertEquals("testFirstName", 
				controller.createUser(validUser).getBody().getFirstName());
		assertEquals("testLastName", 
				controller.createUser(validUser).getBody().getLastName());
		
	}
	
	@Test
	public void getUserInvalidIdCase(){
		
		Mockito.
			when(service.getUserById(ArgumentMatchers.any(Long.class))).
			thenThrow(new UserNotFoundException(new Long(33)));

		try{
			controller.getUser(new Long(33));
		} catch (Exception e){
			assertEquals("No User with the id: 33", e.getMessage());
		}
		
	}
	
	@Test
	public void nullDAOResponseCase(){
		
		Mockito.
			when(service.createUser(ArgumentMatchers.any(User.class))).
				thenReturn(null);
		assertNull(controller.createUser(new User()).getBody());

	}
	
	@Test
	public void exceptionCase(){
		
		Mockito.
			when(service.createUser(ArgumentMatchers.any(User.class))).
				thenThrow(new DataIntegrityViolationException("Foreign key Exception"));
		try{
			controller.createUser(new User());
		} catch (Exception e){
			assertEquals("Foreign key Exception", e.getMessage());
		}
	}
	
	/**
	 * To set the User
	 * @param validUsr
	 */
	private void setValidUser(User validUsr){
		validUsr.setId(new Long(1));
		validUsr.setFirstName("testFirstName");
		validUsr.setLastName("testLastName");
		validUsr.setDateOfBirth(new Date(0));
	}

}
