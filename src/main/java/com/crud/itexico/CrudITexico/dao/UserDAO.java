/**
 * 
 */
package com.crud.itexico.CrudITexico.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.crud.itexico.CrudITexico.Exception.UserNotFoundException;
import com.crud.itexico.CrudITexico.model.User;

/**
 * @author viruskimera
 * The DAO used for Users
 *
 */
@Repository
public interface UserDAO extends JpaRepository<User, Long>{
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public default String deleteUser(final Long id){
		Optional<User> userById = findById(id);
		User user;
		if (userById.isPresent()){
			user = userById.get();
			user.setStatus("INACTIVE");
			save(user);
			return "User with id " + id + " was deleted";
		} else 
			throw new UserNotFoundException(id);
	}
	
	/**
	 * To get a particular USer by its Id
	 * @param id
	 * @return the User
	 */
	public default User getUserById(final Long id){
		Optional<User> userById = findById(id);
		User user;
		if (userById.isPresent()){
			return userById.get();
		} else
			throw new UserNotFoundException(id);
	}
	
	/**
	 * This method is the findAll with status ACTIVE flag
	 * @param status the ACTIVE status
	 * @return the list of active Users
	 */
	public List<User> findByStatus(final String status);
			
	/**
	 * To update basic User info
	 * @param id The user id
	 * @param inUser The new User info
	 * @return
	 */
	public default User updateUser(final Long id, final User inUser){
		Optional<User> userById = findById(id);
		User user;
		if (userById.isPresent()){
			user = userById.get();
			if (inUser.getFirstName() != null)
				user.setFirstName(inUser.getFirstName());
			if (inUser.getLastName() != null)
				user.setLastName(inUser.getLastName());
			if (inUser.getDateOfBirth() != null)
				user.setDateOfBirth(inUser.getDateOfBirth());
			return save(user);
		}else
			throw new UserNotFoundException(id);
	}

}
